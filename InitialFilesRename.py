# Assumption: That this is run from within the oex_subtitle_fixer sub-module sub-folder which is at the same directory level as the decompressed 'course' folder
#
# Usage 1: python3 InitialFilesRename.py subtitles /path/to/exp4011_windows_kernel_exploitation
# This is to delete one old instance of each subtitle in the second folder and replace it with a named __v1-en.srt from the first folder
#
# Usage 2: python3 InitialFilesRename.py markdown /path/to/exp4011_windows_kernel_exploitation_instructor/markdown/
# This is to delete one old instance of each markdown in the second folder and replace it with a named "__v1.md" markdown from the first folder
#
# first folder = <path_to_subtitles_to_match> or <path_to_markdown_to_match>
# second folder = "course/static" or "course/markdown"
#
# NOTE: this script will leave lots of other old instances of subtitles and markdown files in the second folder but the cleanup will be done by later by RunBeforeImportToIncrementVersionNumbers.py

import os
import shutil
import re
import sys

if(len(sys.argv) != 3):
    print("Usage: python3 InitialFilesRename.py subtitles <path_to_subtitles_to_match>")
    print("Usage: python3 InitialFilesRename.py markdown <path_to_markdown_to_match>")
    exit()

# Define paths for files to compare
command = sys.argv[1]
cli_arg_path = sys.argv[2]
srt_path = '../course/static'
video_path = '../course/video'
markdown_path = '../course/markdown'
vertical_path = '../course/vertical'

# Function to read first 3 lines of a file
def read_first_3_lines(file_path):
    lines = []
    with open(file_path, 'r') as f:
        lines = [next(f).rstrip() for x in range(3)]
    return lines

# Function to read first 1 line of a file
# For markdown matching.
# ASSUMES that a markdown comment is added like "<!-- 01-intro -->" to be matched on
def read_first_1_lines(file_path):
    lines = []
    #print(file_path)
    try:
        with open(file_path, 'r') as f:
            lines = [next(f).rstrip() for x in range(1)]
    except Exception as e:
        # Suppress error print for now, due to there being a lot of empty markdown files in some classes
        # print("An error occurred:", e)
        pass
    return lines

def read_lines(command, file_path):
    file_lines = []
    if(command == "subtitles"):
        file_lines = read_first_3_lines(file_path)
    if(command == "markdown"):
        file_lines = read_first_1_lines(file_path)
    return file_lines


found = {}
sorted_found = {}

def rename_files(command,file_search_path):

    file_extension = ""
    if(command == "subtitles"):
        file_extension = ".srt"
        pattern = r"__v(\d+)-en\.srt$"
        regex = re.compile(pattern)
    if(command == "markdown"):
        file_extension = ".md"
        pattern = r"__v(\d+)\.md$"
        regex = re.compile(pattern)
   
    # Loop through each file in the first path, including subdirectories
    for root, dirs, files in os.walk(cli_arg_path):
        for cli_arg_path_filename1_presumed_newer in files:
            # Only match files that end in the given file extension
            if not cli_arg_path_filename1_presumed_newer.endswith(file_extension):
                continue

#            print(cli_arg_path_filename1_presumed_newer)
            cli_arg_filename1_presumed_newer_full_path = os.path.join(root, cli_arg_path_filename1_presumed_newer)
            cli_arg_filename1_presumed_newer_full_path_lines = read_lines(command, cli_arg_filename1_presumed_newer_full_path)
            # Check that we were able to at least get the required number of lines
            if( (command == "subtitles" and (len(cli_arg_filename1_presumed_newer_full_path_lines) < 3)) or (command == "markdown" and (len(cli_arg_filename1_presumed_newer_full_path_lines) < 1) ) ):
                # Suppress error for now, as some classes have empty markdown files
                #print(f"Could not read enough lines for comparison in {cli_arg_filename1_presumed_newer_full_path}")
                continue
            match_found = False

            # Loop through each file in the second path
            for course_filename2 in os.listdir(file_search_path):
                course_filename2_full_path  = os.path.join(file_search_path, course_filename2)

                # Skip files that don't end in file_extension
                if course_filename2.endswith(file_extension):
                    file2_lines = read_lines(command, course_filename2_full_path)
                    if( (command == "subtitles" and (len(file2_lines) < 3)) or (command == "markdown" and (len(file2_lines) < 1) ) ):
                        # Suppress error for now, as some classes have empty markdown files
                        print(f"Could not read enough lines for comparison in {course_filename2_full_path }")
                        continue

                    # For subtitles, we can apparently have false positives if we only use the timestamp from [1], but I suspect we'll have false negatives from using only [2]
                    # For markdown, line 0 should match via an explicit markdown comment 
                    #print(f"{cli_arg_filename1_presumed_newer_full_path_lines[0]} vs. {file2_lines[0]}")
                    if ((command == "subtitles" and cli_arg_filename1_presumed_newer_full_path_lines[1] == file2_lines[1] and cli_arg_filename1_presumed_newer_full_path_lines[2] == file2_lines[2]) or (command == "markdown" and cli_arg_filename1_presumed_newer_full_path_lines[0] == file2_lines[0])):
#                        print(f"{cli_arg_path_filename1_presumed_newer} matches {course_filename2}")
                        if(command == "subtitles"):
                            # Check if the input name ends with the "__v(\d)-en.srt" suffix. If so, remove it and replace it with __v1.
                            match = regex.search(cli_arg_path_filename1_presumed_newer)
                            if(match is not None):
                                sanitized_name = regex.sub("", cli_arg_path_filename1_presumed_newer).encode('ascii', 'ignore').decode() + "__v1"
#                                sanitized_name = cli_arg_path_filename1_presumed_newer[:-4].encode('ascii', 'ignore').decode() + "__v1"
                            # Else, just remove the ".srt" suffix and append __v1
                            else:
                                sanitized_name = cli_arg_path_filename1_presumed_newer[:-4].encode('ascii', 'ignore').decode() + "__v1"

                            # Store the result for printing out, in sorted form, after the fact
                            # NOTE: -7 for len("-en.srt") in either randomized-name file or previous "__v(\d)-en.srt" file
                            found[course_filename2[:-7]] = sanitized_name

                            # Only delete the previous file name if it was a GUID-named file.
                            # If it already had a __v suffix, it should be left in place so that RunBeforeImportToIncrementVersionNumbers.py can potentially 
                            # pick up the version number from it, and it will be removed by RunBeforeImportToIncrementVersionNumbers.py instead
                            match = regex.search(course_filename2_full_path)
                            if(match is None):
                                print(f"Removing {course_filename2_full_path }")
                                os.remove(course_filename2_full_path)
                            file_out = course_filename2_full_path.replace(course_filename2[:-7], sanitized_name)

                        if(command == "markdown"):
                            # Check if the input name ends with the "__v(\d).md" suffix. If so, remove it.
                            match = regex.search(cli_arg_path_filename1_presumed_newer)
                            if(match is not None):
                                sanitized_name = regex.sub("", cli_arg_path_filename1_presumed_newer).encode('ascii', 'ignore').decode() + "__v1"
                            # Else, just remove the ".srt" suffix 
                            else:
#                                print(f"('{course_filename2[:-3]}'), ('{cli_arg_path_filename1_presumed_newer[:-3]}')")
                                sanitized_name = cli_arg_path_filename1_presumed_newer[:-3].encode('ascii', 'ignore').decode() + "__v1"

                            # Store the result for printing out, in sorted form, after the fact
                            found[course_filename2[:-3]] = sanitized_name

                            # Only delete the previous file name if it was a GUID-named file.
                            # If it already had a __v suffix, it should be left in place so that RunBeforeImportToIncrementVersionNumbers.py can potentially 
                            # pick up the version number from it, and it will be removed by RunBeforeImportToIncrementVersionNumbers.py instead
                            match = regex.search(course_filename2_full_path)
                            if(match is None):
                                print(f"Removing {course_filename2_full_path }")
                                os.remove(course_filename2_full_path)
                            file_out = course_filename2_full_path.replace(course_filename2[:-3], sanitized_name)

                        # copy the matching human-readable-named file there instead
                        shutil.copy(cli_arg_filename1_presumed_newer_full_path, file_out)

                        print(f"{course_filename2_full_path} (or potentially other more recent file) replaced with {file_out}");

                        match_found = True
                        break

            if not match_found:
                print(f"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
                print(f"No match found for {cli_arg_path_filename1_presumed_newer}")
                print(f"You probably forgot to upload that video to your class! (Or the first line of text changed.) Re-upload it to the class, re-export the class, and try again.")
                print(f"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")

    # Sort the dictionary by the value (the human-readable name) incase we want to print it
    #sorted_found = sorted(found.items(), key=lambda x: x[1])

    #for old_name, original_name in sorted_found:
    #    print(f"('{old_name}', '{original_name}'),")

def replace_strings_in_XML(root_dir):
    sorted_found = sorted(found.items(), key=lambda x: x[1])
    #print("At beginning of replace_strings_in_XML, sorted_found = ")
    #print(sorted_found)
    for subdir, dirs, files in os.walk(root_dir):
        for filename in files:
            if filename.endswith('.xml'):
                filepath = os.path.join(subdir, filename)
                #print(filepath)
                with open(filepath, 'r+') as f:
                    lines = f.readlines()
                    f.seek(0)
                    f.truncate()
                    for line in lines:
                        #print(line)
                        for old_name, new_name in sorted_found:
                            #print(f"('{old_name}', '{new_name}'),")
                            pattern = re.compile(re.escape(old_name))
                            #print(f"Line before {line}")
                            line_before = line
                            line = re.sub(pattern, new_name, line)
                            #print(f"Line after {line}")
                            if(line != line_before):
                                print(f"Replacement of {old_name} with {new_name} made in {filepath}")
                        f.write(line)

if(command == "subtitles"):
    rename_files(command, srt_path)
    replace_strings_in_XML(video_path)

if(command == "markdown"):
    rename_files(command, markdown_path)
    replace_strings_in_XML(vertical_path)

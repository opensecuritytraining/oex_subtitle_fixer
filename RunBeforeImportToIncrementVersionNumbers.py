# Assumption: That this is run from within the oex_subtitle_fixer sub-module sub-folder which is at the same directory level as the decompressed 'course' folder
#
# Usage 1: python3 RunBeforeImportToIncrementVersionNumbers.py in-place
# This should be used on a repository where there have been no changes made to the core class content, 
# only subtitle or markdown changes via merge requests to a repository containing an exported "course" at its root directory,
# and this file within a sub-module within that repository
#
# Usage 2: python3 RunBeforeImportToIncrementVersionNumbers.py with-delete
# This should be used if you have copied a newly-exported version of your course, and it includes ol
# For instance, this would be true if you knew you made 
# This should only ever be run after first running "python3 InitialFilesRename.py subtitles <path>" and optionally "python3 InitialFilesRename.py markdown <path>",
# because this usage assumes that the newest subtitle/markdown files to be preserved are primarily those named __v1 at the time this is run

import sys
import os
import re

if(len(sys.argv) != 2):
    print("Usage: python3 RunBeforeImportToIncrementVersionNumbers.py in-place")
    print("Usage: python3 RunBeforeImportToIncrementVersionNumbers.py with-delete")
    exit()

def update_subtitle_filenames(root_path, max_existing_version):
    # Define the pattern to match filenames
    pattern = r"__v(\d+)-en\.srt$"
    regex = re.compile(pattern)

    # Walk through the directory and its subdirectories
    for dirpath, dirnames, filenames in os.walk(root_path):
        for filename in filenames:
            # Check if the filename matches the pattern
            match = regex.search(filename)
            if match is not None:
                # Extract the number from the filename
#                num = int(match.group(1))

                # Increment the number and update the filename
                new_num = max_existing_version + 1
                new_filename = regex.sub("__v{}-en.srt".format(new_num), filename)

                # Rename the file
                old_path = os.path.join(dirpath, filename)
                new_path = os.path.join(dirpath, new_filename)
                os.rename(old_path, new_path)

def update_markdown_filenames(root_path, max_existing_version):
    # Define the pattern to match filenames
    pattern = r"__v(\d+)\.md$"
    regex = re.compile(pattern)

    # Walk through the directory and its subdirectories
    for dirpath, dirnames, filenames in os.walk(root_path):
        for filename in filenames:
            # Check if the filename matches the pattern
            match = regex.search(filename)
            if match is not None:
                # Extract the number from the filename
#                num = int(match.group(1))

                # Increment the number and update the filename
                new_num = max_existing_version + 1
                new_filename = regex.sub("__v{}.md".format(new_num), filename)

                # Rename the file
                old_path = os.path.join(dirpath, filename)
                new_path = os.path.join(dirpath, new_filename)
                os.rename(old_path, new_path)


###

def increment_strings_within_xml(root_path, max_existing_version):
    # Define the pattern to match strings like "__v1"
    pattern = r"(__v\d+)\b"
    regex = re.compile(pattern)

    # Walk through the directory and its subdirectories
    for dirpath, dirnames, filenames in os.walk(root_path):
        for filename in filenames:
            # Check if the file is an XML file
            if filename.endswith(".xml"):
                file_path = os.path.join(dirpath, filename)

                # Open the file for reading and writing
                with open(file_path, "r+") as f:
                    # Read the file content into memory
                    lines = f.readlines()

                    # Iterate over each line in the file
                    for i, line in enumerate(lines):
                        # Use regex to find matches in the line
                        matches = regex.findall(line)

                        # Iterate over each match and increment the number
                        for match in matches:
                            #num = int(match[3:])  # Extract the number from the match
                            new_num = max_existing_version + 1
                            new_match = "__v" + str(new_num)
                            line = line.replace(match, new_match)

                        # Update the line in the list of lines
                        lines[i] = line

                    # Write the updated lines back to the file
                    f.seek(0)
                    f.writelines(lines)
                    f.truncate()

###

def increment_strings_within_assets_json(root_path, max_existing_version):
    # Define the pattern to match strings like "__v1"
    pattern = r"(__v\d+)\b"
    regex = re.compile(pattern)

    file_path = os.path.join(root_path, "assets.json")

    # Open the file for reading and writing
    with open(file_path, "r+") as f:
        # Read the file content into memory
        lines = f.readlines()

        # Iterate over each line in the file
        for i, line in enumerate(lines):
            # Use regex to find matches in the line
            matches = regex.findall(line)

            # Iterate over each match and increment the number
            for match in matches:
                #num = int(match[3:])  # Extract the number from the match
                new_num = max_existing_version + 1
                new_match = "__v" + str(new_num)
                line = line.replace(match, new_match)

            # Update the line in the list of lines
            lines[i] = line

        # Write the updated lines back to the file
        f.seek(0)
        f.writelines(lines)
        f.truncate()

def return_largest_version(course_path):
    max_version_so_far = 1;
    tmp_version = 1;
    # Define the pattern to match strings like "__v1"
    pattern = r"__v(\d+)"
    regex = re.compile(pattern)

    # Walk through the directory and its subdirectories
    for dirpath, dirnames, filenames in os.walk(course_path):
        for filename in filenames:
            match = regex.search(filename)
            if (match is not None):
                # Extract the number from the filename
                tmp_version = int(match.group(1))
#                print(tmp_version)
                if( tmp_version > max_version_so_far ):
                    max_version_so_far = tmp_version
    return max_version_so_far

def delete_unnecessary_files(course_path):
    # Define the pattern to match strings like "__v[number]"
    pattern = r"__v(\d+)"
    regex = re.compile(pattern)

    # Walk through the directory and its subdirectories
    for dirpath, dirnames, filenames in os.walk(course_path):
        for filename in filenames:
            match = regex.search(filename)
            if (match is not None):
                # Extract the number from the filename
                tmp_version = int(match.group(1))
                # This code assumes that you've run InitialFilesRename.py, and thus all the files we actually want are now named __v1
                if(tmp_version != 1):
                    full_path_to_remove = os.path.join(course_path, filename)
#                    print(f"delete {full_path_to_remove}")
                    os.remove(full_path_to_remove)

##### END HELPER FUNCTIONS, BEGIN CODE

# Find the maximum number currently used in the course
version1 = return_largest_version("../course/markdown/")
version2 = return_largest_version("../course/static/")
max_existing_version = max(version1, version2)
print(f"max_existing_version = {max_existing_version}, so new version will be {max_existing_version+1}")

if(sys.argv[1] == "with-delete"):
    # Delete all files that match __v{\d) != __v1
    delete_unnecessary_files("../course/static/")
    delete_unnecessary_files("../course/markdown/")

# Call the function to update the filenames in a directory
update_subtitle_filenames("../course/static/", max_existing_version)
update_markdown_filenames("../course/markdown/", max_existing_version)

# Call the function to increment the numbers in a directory
increment_strings_within_xml("../course/video/", max_existing_version)
increment_strings_within_xml("../course/vertical/", max_existing_version)

# SUBSEQUENTLY FOUND TO BE UNNECESSARY
# Also update the policies/assets.json file references
increment_strings_within_assets_json("../course/policies/", max_existing_version)

import tarfile
import os.path

# Usage: python3 TarGzCourse.py
# Usage: This will output 'upload.tar.gz' which can be used via the Import interface in Open edX

def make_tarfile(output_filename, source_dir):
    with tarfile.open(output_filename, "w:gz") as tar:
        tar.add(source_dir, arcname=os.path.basename(source_dir))

make_tarfile("upload.tar.gz", "../course")

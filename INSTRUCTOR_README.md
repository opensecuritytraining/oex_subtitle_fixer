<!-- vim-markdown-toc GFM -->

- [BEFORE YOU BEGIN](#before-you-begin)
- [NOTE ABOUT SJSON FILES](#note-about-sjson-files)
- [Initial usage for InitialFilesRename.py:](#initial-usage-for-initialsubtitlerenamepy)
- [Subsequent usage for InitialFilesRename.py:](#subsequent-usage-for-initialsubtitlerenamepy)
- [Usage for RunBeforeImportToIncrementVersionNumbers.py:](#usage-for-runbeforeimporttoincrementversionnumberspy)
  - [Case 1 ("simple case"): There have been no changes to the contents of the web page between last check in, and new subtitle update:](#case-1-simple-case-there-have-been-no-changes-to-the-contents-of-the-web-page-between-last-check-in-and-new-subtitle-update)
  - [Case 2 ("complex case"): There have been changes to the contents of the web page between last check in, *and* new subtitle updates from merge requests that you want to integrate:](#case-2-complex-case-there-have-been-changes-to-the-contents-of-the-web-page-between-last-check-in-and-new-subtitle-updates-from-merge-requests-that-you-want-to-integrate)
- [Final Step](#final-step)

<!-- vim-markdown-toc -->

# BEFORE YOU BEGIN

The first time you attempt these instructions, you should not attempt them on your primary class on beta.ost2.fyi! Rather, you should ***create a new course with some placeholder name.***

Export your existing course via the `Open edX (OEX) Studio -> Your Class -> Tools -> Export interface`. 

Then proceed with the instructions below. Once you confirm you can make bulk changes successfully on your placeholder course, then you can proceed to undertake the steps on your main course (backing it up one more time *just in case*, **and probably also asking Xeno to make a VM snapshot of the server, just in case.** (useful because OEX does not always behave as expected, e.g. it keeps old static files like subtitles even when importing a course) )

# NOTE ABOUT SJSON FILES

If you ever click the "refresh" style circular arrow next to the Video ID field while editing a video in OEX:

![](img/oex_refresh.png)

It will convert the .srt subtitle file into a .sjson subtitle file.  

Unfortunately the below scripts cannot handle .sjson files. If you see that your exported class has .sjson files in course/static, you will need to  
1) open the file to identify which video it corresponds to  
2) Go back to OEX, copy the YouTube URL, save the video, *delete the video* via hitting the trash icon, and hit Publish.  
3) Add a new video, add the previous YouTube URL, and upload the previous .srt file. Then hit Publish  
4) Repeat for all .sjson files, and then re-export the class and continue again from below.

NOTE: it could also be the .sjson files are old versions of subtitles and are not used anymore. In this case, they can be left as is and will just be ignored anyway by our scripts.

# Initial usage for InitialFilesRename.py: 

These steps are done the first time you are establishing a repository to store your exported class (to allow students to update subtitles in place). 

1) Use the Open edX Export functionality from the instructor studio class page to export the latest version of the class, naming it `latest_class_DATE_vX.tar.gz`

2) Create a folder `~/tmp0/repo_name` (e.g. `~/tmp0/exp4011_oex_course` for a proper class repository that will be used with students, but I used `exp4011_bulk_change_test` in my initial testing below)

3) Copy `latest_class_DATE_vX.tar.gz` to `~/tmp0/repo_name`, and then untar/gz it

4) You should now have `~/tmp0/repo_name/course`

5) `cd ~/tmp0/repo_name/`

6) `git clone https://gitlab.com/opensecuritytraining/oex_course_fixer`

7) `cd oex_course_fixer`

8) Execute the following command, where `<path_to_subtitles_to_match>` is any folder full of named subtitles. (But in practice it may be your previous git repository folder where subtitles were stored before this procedure was enacted.)
```
python3 InitialFilesRename.py subtitles <path_to_subtitles_to_match>
```

E.g. within `~/tmp0/exp4011_bulk_change_test` I ran:
```
python3 InitialFilesRename.py subtitles /Users/xeno/Desktop/OST2/__OST2_Material/Exp4011/exp4011_windows_kernel_exploitation/subtitles
```

This will replace the GUIDed-named files with human-readable file names from `<path_to_subtitles_to_match>`. It will also update files in `course/video/*.xml` that may contain references to the old GUID files.

If the source subtitles are spread around, `InitialFilesRename.py` can be run multiple times on multiple paths. But ultimately it only needs to be run once before the first git checkin.

You may get errors about files from your `<path_to_subtitles_to_match>` not matching anything from your `course/static` folder. This could be because you forgot to upload the file. But it could also be because Open edX normalizes uploaded srt files so that all text is on a single line. So if the first couple lines of text have been moved from multiple lines to one, that will yield a false positive. The only way currently to tell whether this is a false positive is just to go confirm that the file has been uploaded, and if so, whether the original file was multi-line. If the original file is multi-line, and the normalized is not, the easiest way to resolve it is to just change the version in `<path_to_subtitles_to_match>` to not be multi-line. Then delete your `~/tmp0/repo_name/course`, untar/gz the exported course, and try again, until you see no errors.

Also double check that every file in course/static has been successfully renamed. If there is any file name that is still based on a GUID, determine whether that is because it does not exist in `<path_to_subtitles_to_match>`, and if so, make sure it is added to `<path_to_subtitles_to_match>`, with an appropriate human-readable name that matches the video name (so students can find it).

It is now a formal assumption of the scripts that if you ever run InitialFilesRename.py again on the `course/static` folder, all files in that folder must be human-readable-named files. So make sure you get all files renamed before proceeding.

9) Optional: There is also support for renaming markdown files and then managing them from this same `repo_name` repository. If you want to manage Markdown files this way, execute the following command, where <path_to_markdown_to_match> is any folder full of named markdown files.
```
python3 InitialFilesRename.py markdown <path_to_markdown_to_match>
```

As with subtitles, all files must be successfully renamed to human-readable file names before proceeding, because the `InitialFilesRename.py` script will not work correctly if you try to run it again on the `course/markdown` folder as input if not all files were renamed.

10) You should now be able to check in `~/tmp0/repo_name` to a git repository via normal gitlab instructions. E.g. from `~/tmp0/exp4011_bulk_change_test` I did the following, after first creating the repository on gitlab:   

```
rm -rf oex_course_fixer
git init
git remote add origin git@gitlab.com:opensecuritytraining/exp4011_bulk_change_test.git
git add .
git submodule add git@gitlab.com:opensecuritytraining/oex_course_fixer.git
git commit -am "Initial commit"
git push --set-upstream origin master
```


# Subsequent usage for InitialFilesRename.py:

If you add a new video to your class, then the next time you export the class, that video will have GUID-named subtitles. Therefore you will need to run `InitialFilesRename.py` again to find and replace the GUID-name with the human-readable name.

You should then copy the renamed subtitle files from `course/static/` into the same path in your git repo, and add the new ones. And you should also copy all the files from `course/video` into your git repository and add the new ones.


# Usage for RunBeforeImportToIncrementVersionNumbers.py: 

## Case 1 ("simple case"): There have been no changes to the contents of the web page between last check in, and new subtitle update:  

* After a merge request occurs that changes the subtitles, the local git repository should be updated via a pull

* From this directory, run `python3 RunBeforeImportToIncrementVersionNumbers.py`. This will update all the files named like `__v1` to `__v2`. It will also update references within XML files.

* GOTO Final Step below.


## Case 2 ("complex case"): There have been changes to the contents of the web page between last check in, *and* new subtitle updates from merge requests that you want to integrate:  

* (First of all, you can try to avoid this complex case in general, by always exporting the class after every time you make changes, and then checking it in to this repository. 
That way, by the time merge requests come in, hopefully they're merging into the latest version of the class material anyway, so it would be appropriate to use the simple case above. 
However, that is generally expected to be too onerous in practice during beta testing, if lots of changes are being made (but not necessarily after the class is released).
But you can be the judge, based on the steps you have to take below, if you don't take that approach.)

* *NOTE*: This procedure doesn't handle if you added more videos with new subtitle files to your class. For that, see "Subsequent usage for InitialFilesRename.py" above.

1) Accept the merge request

2) Git pull the changes to your local copy of the repository (e.g. let's assume it's at `~/tmp1/exp4011_bulk_change_test`), noting the changed srt file paths

e.g. 
```
Fast-forward
 course/static/_Exp4011_01_Introduction__v2-en.srt | 2 +-
 1 file changed, 1 insertion(+), 1 deletion(-)
```

3) Use the Open edX Export functionality from the instructor studio class page to export the latest version of the class, naming it `latest_class_DATE_vX.tar.gz`

4) Create a new temporary directory `~/tmp2/`, and copy `latest_class_DATE_vX.tar.gz` into it.

5) Untar/gz the file, so that you now have ~/tmp2/course

6) `cp -r ~/tmp1/exp4011_bulk_change_test/oex_course_fixer ~/tmp2/oex_course_fixer`

7) ***IMPORTANT*** If you look in `~/tmp2/course/static`, you will see that there are now the existing versioned subtitle files (e.g. `__v2-en.srt`), but also the original GUID-named subtitle files 
(like `0cdb3d70-7889-4186-891f-f8ad7de038b7-en.srt`). This is due to Open edX kind of sucking, and keeping around old files that are not referenced anywhere when importing a new class...
**Manually delete all the files that are not at the latest version number** (e.g. if the latest is `__v4` you would delete all the GUIDed and `__v2`, and `__v3`.)

8) Copy the changed files from the merge request (e.g. `~/tmp1/exp4011_bulk_change_test/course/static/_Exp4011_01_Introduction__v2-en.srt`) to the new directory (e.g. `~/tmp2/course/static/_Exp4011_01_Introduction__v2-en.srt`)

9) From  `~/tmp2/oex_course_fixer`, run `python3 RunBeforeImportToIncrementVersionNumbers.py` to increment the latest version number (e.g. v2 -> v3)

10) Replace the git repo's course folder with the new course folder (with update subtitles)
```
rm -rf ~/tmp1/exp4011_bulk_change_test/course`
cp -r ~/tmp2/course ~/tmp1/exp4011_bulk_change_test/course`
```

11) from within `~/tmp1/exp4011_bulk_change_test`, run `git add course`

12) `git status`. You should see all the subtitle file version numbers updated, and also all the corresponding `course/video/*.xml` files updated to reference the correct subtitle file. You should also see any new changes that are the result of you having updated the course elsewhere (e.g. in `course/sequential/` or `course/vertical`)

13) `git commit -am "Updated version from vX to vY"`

14) `git push`

* GOTO Final Step below.

# Final Step

* From `~/tmp1/exp4011_bulk_change_test/oex_course_fixer` (and only that directory, so you get the hierarchy right), run `rm upload.tar.gz` (if needed) and `python3 TarGzCourse.py`. This will create a new `upload.tar.gz` file in that directory.

* Then use the Import functionality in Open edX to import `upload.tar.gz` and overwrite the entire class (which will subsequently update all the subtitles)
